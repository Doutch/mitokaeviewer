# 概要
ゆりよつ さん（[@yuriyotu](https://twitter.com/yuriyotu?s=17)）が考えたミトとカエデの画像認識アプリをそれっぽく動くように仕立て上げたWindowsアプリケーションです  （[元ネタ](https://twitter.com/yuriyotu/status/1149694024963379200?s=20)はこちら）  
カメラで撮影した画像をGoogle Vision APIで解析し、その結果をミトさんが教えてくれます  
画像素材は全てゆりよつさんのTwitterよりお借りしています（ありがとうございます）  

# どんなアプリなの？
使用動画をTwitterに上げてるので[そちら](https://twitter.com/strelkar/status/1150239182029123585?s=20)を御覧ください  
そもそもミトとカエデってなに？？って方は是非Twitterで検索してみて下さい  

# 自分の環境で動く？
Android、iOSアプリではなく**Windowsアプリ**です  
Surface Pro 6（Windows 10 Home 64bit)で動作確認済みです  
WEBカメラが接続済みのWindows環境なら多分動くかと思います  
なお動作には.NET Framework 4.6.2以上が必要なので、必要に応じて[インストール](https://support.microsoft.com/ja-jp/help/3151800/the-net-framework-4-6-2-offline-installer-for-windows)してあげて下さい  
最近のWindows 10なら既に入っているかも

# どんなことをやってるの？
OpenCVを使ってカメラよりキャプチャした画像を基に、このあたりのAPIを利用して検出処理を行っています  
簡単に今回やりたいことが行える便利なAPI達ですが、基本有料サービスかつ無料枠の範囲でもクレカの登録が必須なので使用ハードルは少し高いかもしれません
* Google Vision API
    * 今回のアプリのキモの物体認識に使用
    * 物体の識別、位置の推定、確度（スコア）の算出まで一括で行ってくれます
* Google Cloud Translation API
    * 物体認識の結果が英語表記なので、それを日本語訳するのに使用
    * 出力先の言語種別（日本語）を指定するだけで翻訳してくれます

# どうやったら使えるの？
Google Developers Consoleより各APIの有効化とクレカの登録等の作業が必須となります  
Googleの解説ページを参考に設定を行って下さい（丸投げ）

1. 初期セットアップ～Vison APIの有効化  
https://cloud.google.com/vision/docs/quickstart-client-libraries?hl=ja  
2. Cloud Translation APIの有効化  
既にPJを作成済みなので、ここでは「プロジェクトをセットアップする」より1で作成したPJを選択するだけでOKです  
https://cloud.google.com/translate/docs/quickstart-client-libraries?hl=ja
3. 実行ファイルをDLする  
[ここから](https://gitlab.com/Doutch/mitokaeviewer/-/releases)zipファイルをダウンロードして適当な場所に解凍して下さい  
あとはexeを実行すればアプリが起動します  
おつでろーんでした

**メモ**  
PC上にサービスアカウントキーファイルを配置してゴニョゴニョするよう指示されますが、作者の場合だと以下のように設定しました
* DLしたJSONファイルを以下のフォルダに配置（無いフォルダは任意に作成）  
    `C:\Users\<USER_NAME>\AppData\Local\Google\GoogleApi`
* 環境変数にJSONファイルのパスを追加
    1. Windowsのコントロールパネルで「環境変数」で検索
    2. ユーザーの環境変数 > 新規
    3. 変数名に「GOOGLE_APPLICATION_CREDENTIALS」、変数値にJSONファイルのパスを入力してOK

# じゃあ使ってみよう
1. 物体を認識させる  
赤色のカメラボタンをクリックするとプレビューが一時停止し、写っている物体の解析が実行されます  
物体認識に成功すると、画像中に物体を囲う枠とスコアが表示されるとともに、ミトちゃんがその物体が何なのか教えてくれます  
再度クリックするとまたプレビューを再開します  
2. 画像を保存する  
きろくボタンをクリックすると、最後に認識した画像をpng形式で保存します  
保存先はEXEと同位置のSavedImagesフォルダです

# Option：各種パラメータを変更する
EXEと同位置にある「MitoKaeViewer.exe.config」を編集する事でパラメータを変更可能です  
次回起動時より反映されます
```xml
  <userSettings>
    <MitoKaeViewer.Properties.Settings>
      <setting name="CaptureResolutionWidth" serializeAs="String">
        <value>1920</value>
      </setting>
      <setting name="CaptureResolutionHeight" serializeAs="String">
        <value>1080</value>
      </setting>
      <setting name="RefreshSpanMs" serializeAs="String">
        <value>10</value>
      </setting>
      <setting name="UseCameraSource" serializeAs="String">
        <value>1</value>
      </setting>
      <setting name="RectThickness" serializeAs="String">
        <value>3</value>
      </setting>
    </MitoKaeViewer.Properties.Settings>
  </userSettings>
```
* CaptureResolutionWidth
    * キャプチャ画像を取得する際の解像度設定（横幅）です
* CaptureResolutionHeight
    * キャプチャ画像を取得する際の解像度設定（縦幅）です
* RefreshSpanMs
    * プレビューの更新頻度をmsで指定可能です
    * 基本はこのままで良いかも
* UseCameraSource
    * OpenCVで使用するカメラの識別値です（0～）
    * フロント・リア等で複数カメラがあるPCの場合は、この数値を変えることで使用するカメラを変更可能です（参考：Surface Pro 6 -> フロント=0、リア=1）
* RectThickness
    * 物体認識枠の太さの設定値