﻿using System.Collections.Generic;

namespace MitoKaeViewer.Services
{
    public interface IObjectFindService : System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// 検出した物体情報
        /// </summary>
        List<ObjectInfo> DetectedItems { get; set; }

        /// <summary>
        /// 物体検出を行う
        /// </summary>
        /// <param name="buffer">画像バッファ</param>
        /// <param name="drawImgWidth">検出位置を描画する先の画像サイズ（横幅）</param>
        /// <param name="drawImgHeight">検出位置を描画する先の画像サイズ（縦幅）</param>
        void FindObjects(byte[] buffer, int drawImgWidth, int drawImgHeight);
    }
}