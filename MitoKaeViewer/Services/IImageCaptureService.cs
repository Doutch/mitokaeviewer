﻿using System;
using System.Threading.Tasks;
using OpenCvSharp;

namespace MitoKaeViewer.Services
{
    public interface IImageCaptureService : System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// キャプチャの実行状態
        /// </summary>
        bool IsCaptureRunning { get; set; }
        /// <summary>
        /// キャプチャ停止時に最後に取得したフレーム
        /// </summary>
        Mat LastCapturedMat { get; set; }

        /// <summary>
        /// フレーム更新イベント
        /// </summary>
        event EventHandler<FrameUpdateEventArgs> FrameUpdateEvent;

        /// <summary>
        /// キャプチャ開始
        /// </summary>
        /// <returns></returns>
        Task Start();
        /// <summary>
        /// キャプチャ停止
        /// </summary>
        void Stop();
    }
}