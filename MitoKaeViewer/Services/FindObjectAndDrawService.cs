﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
using OpenCvSharp.Extensions;
using System.Windows.Media.Imaging;
using System.IO;

namespace MitoKaeViewer.Services
{
    public class FindObjectAndDrawService : Prism.Mvvm.BindableBase, IFindObjectAndDrawService
    {
        #region Properties
        private WriteableBitmap _DrewImage;
        /// <summary>
        /// 検出後描画画像
        /// </summary>
        public WriteableBitmap DrewImage
        {
            get { return _DrewImage; }
            set { SetProperty(ref _DrewImage, value); }
        }
        private string _ObjectName;
        /// <summary>
        /// 検出した物体名
        /// </summary>
        public string ObjectName
        {
            get { return _ObjectName; }
            set { SetProperty(ref _ObjectName, value); }
        }
        #endregion
        private IObjectFindService _FindService;
        private IImageDrawService _DrawService;
        private int _RectThickness;

        public FindObjectAndDrawService(IObjectFindService findService, IImageDrawService drawService, int rectThickness)
        {
            _FindService = findService;
            _DrawService = drawService;
            _RectThickness = rectThickness;
        }

        /// <summary>
        /// 物体検出と検出位置の描画を行う
        /// </summary>
        /// <param name="mat">検出＆描画対象の画像データ</param>
        public void FindAndDraw(Mat mat)
        {
            ObjectInfo findRes = null;

            //負荷軽減の為に1/4にリサイズ
            var newSize = new Size(mat.Width / 4, mat.Height / 4);
            using (var smallImg = new Mat(newSize, mat.Type()))
            {
                Cv2.Resize(mat, smallImg, newSize);

                //物体検出
                _FindService.FindObjects(smallImg.ToBytes(), mat.Width, mat.Height);
                findRes = _FindService.DetectedItems.FirstOrDefault(); //スコアが最上位の物をチョイス
            }           

            //物体位置描画
            if (findRes != null)
            {
                _DrawService.DrawRect(mat, findRes, _RectThickness);
                ObjectName = findRes.Name;
            }
            else
            {
                ObjectName = "???"; //発見失敗時
            }

            //表示更新
            DrewImage = WriteableBitmapConverter.ToWriteableBitmap(mat);
        }

        /// <summary>
        /// 最後に認識した画像を保存する
        /// </summary>
        /// <param name="dirPath">保存先ディレクトリパス</param>
        public void SaveLastDrewImage(string dirPath)
        {
            if(DrewImage == null)
            {
                throw new Exception("画像データが存在しない");
            }

            if (!Directory.Exists(dirPath))
            {
                Directory.CreateDirectory(dirPath);
            }

            var savePath = $"{dirPath}{DateTime.Now.ToString("yyyyMMdd_HHmmss")}.png";

            //png形式で保存
            using (var fs = new FileStream(savePath, FileMode.Create, FileAccess.Write))
            {
                var enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(DrewImage));
                enc.Save(fs);
            }
        }
    }
}
