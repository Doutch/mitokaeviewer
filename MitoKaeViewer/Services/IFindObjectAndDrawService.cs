﻿using System.Windows.Media.Imaging;
using OpenCvSharp;

namespace MitoKaeViewer.Services
{
    public interface IFindObjectAndDrawService : System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// 検出後描画画像
        /// </summary>
        WriteableBitmap DrewImage { get; set; }
        /// <summary>
        /// 検出した物体名
        /// </summary>
        string ObjectName { get; set; }

        /// <summary>
        /// 物体検出と検出位置の描画を行う
        /// </summary>
        /// <param name="mat">検出＆描画対象の画像データ</param>
        void FindAndDraw(Mat mat);
        /// <summary>
        /// 最後に認識した画像を保存する
        /// </summary>
        /// <param name="dirPath">保存先ディレクトリパス</param>
        void SaveLastDrewImage(string path);
    }
}