﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Google.Cloud.Vision.V1;
using Google.Protobuf.Collections;
using Google.Cloud.Translation.V2;

namespace MitoKaeViewer.Services
{
    public class ObjectFindService : Prism.Mvvm.BindableBase, IObjectFindService
    {
        #region Properties
        private List<ObjectInfo> _DetectedItems;
        /// <summary>
        /// 検出した物体情報
        /// </summary>
        public List<ObjectInfo> DetectedItems
        {
            get { return _DetectedItems; }
            set { SetProperty(ref _DetectedItems, value); }
        }
        #endregion
        private ImageAnnotatorClient _VisionClient;
        private TranslationClient _TranslationClient;

        public ObjectFindService()
        {
            _VisionClient      = ImageAnnotatorClient.Create();
            _TranslationClient = TranslationClient.Create();
        }

        /// <summary>
        /// 物体検出を行う
        /// </summary>
        /// <param name="buffer">画像バッファ</param>
        /// <param name="drawImgWidth">検出位置を描画する先の画像サイズ（横幅）</param>
        /// <param name="drawImgHeight">検出位置を描画する先の画像サイズ（縦幅）</param>
        public void FindObjects(byte[] buffer, int drawImgWidth, int drawImgHeight)
        {
            var image     = Image.FromBytes(buffer);
            var responses = _VisionClient.DetectLocalizedObjects(image, null, 10); //最大10件

            var res = new List<ObjectInfo>();
            foreach (var annotation in responses.OrderByDescending(x => x.Score)) //スコアの降順で返却
            {
                var roi = ConvertToRect(drawImgWidth, drawImgHeight, annotation.BoundingPoly.NormalizedVertices);
                res.Add(new ObjectInfo(
                    TransrateName(annotation.Name), //英語表記を日本語表記に変換
                    annotation.Score,
                    roi));
            }

            DetectedItems = res;
        }

        /// <summary>
        /// Vision APIの検出位置情報を画像上の位置情報に変換する
        /// </summary>
        /// <remarks>Vision APIの戻り値は相対位置表記の為、絶対位置に直す必要がある</remarks>
        /// <param name="imgWidth">画像横幅</param>
        /// <param name="imgHeight">画像縦幅</param>
        /// <param name="positions">Vision APIの検出位置情報</param>
        /// <returns>画像上の検出位置情報</returns>
        private System.Drawing.Rectangle ConvertToRect(int imgWidth, int imgHeight, RepeatedField<NormalizedVertex> positions)
        {
            if (positions.Count < 4)
                throw new ArgumentException("座標データ不足");

            var leftTopRaw     = positions.OrderBy(x => x.X).ThenBy(y => y.Y).First();
            var rightBottomRaw = positions.OrderByDescending(x => x.X).ThenByDescending(y => y.Y).First();

            var leftTop     = new System.Drawing.Point((int)(leftTopRaw.X * imgWidth), (int)(leftTopRaw.Y * imgHeight));
            var rightBottom = new System.Drawing.Point((int)(rightBottomRaw.X * imgWidth), (int)(rightBottomRaw.Y * imgHeight));

            return new System.Drawing.Rectangle(
                leftTop.X, leftTop.Y,
                rightBottom.X - leftTop.X, rightBottom.Y - leftTop.Y
                );
        }

        /// <summary>
        /// 英語表記を日本語表記に翻訳する
        /// </summary>
        /// <param name="enName">英語表記名</param>
        /// <returns>日本語表記名</returns>
        private string TransrateName(string enName)
        {
            return _TranslationClient.TranslateText(enName, "jpn").TranslatedText;
        }
    }

    /// <summary>
    /// 物体検出結果
    /// </summary>
    public class ObjectInfo
    {
        /// <summary>
        /// 物体名
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 検出位置
        /// </summary>
        public System.Drawing.Rectangle Roi { get; set; }
        /// <summary>
        /// スコア（0～1）
        /// </summary>
        public double Score { get; set; }

        public ObjectInfo(string name, double score, System.Drawing.Rectangle roi)
        {
            Name  = name;
            Score = score;
            Roi   = roi;
        }
    }
}
