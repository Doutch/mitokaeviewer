﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenCvSharp;
using System.Windows.Media.Imaging;
using System.Drawing;

namespace MitoKaeViewer.Services
{
    public class ImageDrawService : Prism.Mvvm.BindableBase, IImageDrawService
    {
        #region Properties
        private WriteableBitmap _DrewImage;
        /// <summary>
        /// 描画画像
        /// </summary>
        public WriteableBitmap DrewImage
        {
            get { return _DrewImage; }
            set { SetProperty(ref _DrewImage, value); }
        }
        #endregion

        /// <summary>
        /// 指定位置に矩形とスコア情報をを描画する
        /// </summary>
        /// <param name="mat">描画先の画像</param>
        /// <param name="objectInfo">検出情報</param>
        /// <param name="rectThickness">描画枠の太さ</param>
        public void DrawRect(Mat mat, ObjectInfo objectInfo, int rectThickness = 3)
        {
            //ROIを描画
            var roi = objectInfo.Roi;
            Cv2.Rectangle(mat, new Rect(roi.X, roi.Y, roi.Width, roi.Height), Scalar.White, rectThickness);

            //Scoreを描画
            var score = (int)(objectInfo.Score * 100);
            Cv2.PutText(mat,
                $"{score}%",
                new OpenCvSharp.Point(roi.X + rectThickness, roi.Y + roi.Height - rectThickness), //枠の左下に表示
                HersheyFonts.HersheySimplex,
                1,
                Scalar.Pink,
                2);
        }
    }
}
