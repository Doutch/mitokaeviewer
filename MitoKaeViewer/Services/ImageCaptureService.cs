﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using System.Threading;
using OpenCvSharp;
using OpenCvSharp.Extensions;

namespace MitoKaeViewer.Services
{
    public class ImageCaptureService : Prism.Mvvm.BindableBase, IImageCaptureService
    {
        #region Properties
        private Mat _LastCapturedMat;
        /// <summary>
        /// キャプチャ停止時に最後に取得したフレーム
        /// </summary>
        public Mat LastCapturedMat
        {
            get { return _LastCapturedMat; }
            set { SetProperty(ref _LastCapturedMat, value); }
        }
        private bool _IsCaptureRunning;
        /// <summary>
        /// キャプチャの実行状態
        /// </summary>
        public bool IsCaptureRunning
        {
            get { return _IsCaptureRunning; }
            set { SetProperty(ref _IsCaptureRunning, value); }
        }
        #endregion

        /// <summary>
        /// フレーム更新イベント
        /// </summary>
        public event EventHandler<FrameUpdateEventArgs> FrameUpdateEvent;

        private WriteableBitmap _CaptureFrame;
        private int _WaitSpcanMs;
        private int _CaptureSourceIndex;
        private CancellationTokenSource _TokenSource;
        private double _CaptureHeightRatio;
        private int _CaptureWidth;
        private int _CaptureHeight;

        public ImageCaptureService(int capWidth, int capHeight, double capHeightRatio, int waitSpan, int captureSourceIndex = 0)
        {
            _WaitSpcanMs = waitSpan;
            _CaptureSourceIndex = captureSourceIndex;
            _CaptureHeightRatio = capHeightRatio;
            _CaptureWidth = capWidth;
            _CaptureHeight = capHeight;
        }

        /// <summary>
        /// キャプチャ開始
        /// </summary>
        /// <returns></returns>
        public async Task Start()
        {
            //既に実行中なら古い方をStop
            if (IsCaptureRunning)
                Stop();
            IsCaptureRunning = true;

            _TokenSource = new CancellationTokenSource();

            //カメラ準備
            var capture = new VideoCapture(_CaptureSourceIndex)
            {
                FrameWidth = _CaptureWidth,
                FrameHeight = _CaptureHeight
            };
            //Cropサイズ設定
            var cropRoi = CalcCropRoi(_CaptureHeightRatio, capture.FrameWidth, capture.FrameHeight);

            //節約のために画像を使い回す
            _CaptureFrame = new WriteableBitmap(
                cropRoi.Width, cropRoi.Height,
                96, 96,
                System.Windows.Media.PixelFormats.Bgr24, null);

            using (var matFrame = new Mat())
            {
                await CaptureFrameAsync(capture, matFrame);
                
                //キャプチャループ
                while (true)
                {
                    using (var cropMat = await CropFrameAsync(matFrame, cropRoi))
                    {
                        if (_TokenSource.IsCancellationRequested)
                        {
                            LastCapturedMat = cropMat.Clone();
                            break;
                        }

                        WriteableBitmapConverter.ToWriteableBitmap(cropMat, _CaptureFrame);
                        FrameUpdateEvent(this, new FrameUpdateEventArgs(_CaptureFrame));

                        await CaptureFrameAsync(capture, matFrame);
                    }

                    //wait
                    await Task.Delay(_WaitSpcanMs);
                }
                IsCaptureRunning = false;
            }
        }

        /// <summary>
        /// キャプチャ停止
        /// </summary>
        public void Stop()
        {
            if(_TokenSource != null)
                _TokenSource.Cancel();
        }

        /// <summary>
        /// クロップ範囲を計算する
        /// </summary>
        /// <param name="heightRatio">横幅を1とした時の縦幅の比</param>
        /// <param name="imgWidth">ソース画像の横幅</param>
        /// <param name="imgHeight">ソース画像の縦幅</param>
        /// <returns>クロップ範囲</returns>
        private Rect CalcCropRoi(double heightRatio, int imgWidth, int imgHeight)
        {
            int cropWidth, cropHeight;
            if (heightRatio < 1)
            {
                //横長
                cropHeight = (int)(imgWidth * heightRatio);
                cropWidth = imgWidth;
            }
            else
            {
                //縦長
                cropHeight = imgHeight;
                cropWidth = (int)(imgHeight / heightRatio);
            }

            var cropOffsetX = (imgWidth - cropWidth) / 2;
            var cropOffsetY = (imgHeight - cropHeight) / 2;

            return new Rect(cropOffsetX, cropOffsetY, cropWidth, cropHeight);
        }

        private async Task<bool> CaptureFrameAsync(VideoCapture capture, Mat container)
        {
            return await Task.Run(() => { return capture.Read(container); });
        }

        private async Task<Mat> CropFrameAsync(Mat source, Rect roi)
        {
            return await Task.Run(() => { return source.Clone(roi); });
        }
    }

    /// <summary>
    /// フレーム更新時イベント情報
    /// </summary>
    public class FrameUpdateEventArgs : EventArgs
    {
        /// <summary>
        /// 取得フレーム
        /// </summary>
        public WriteableBitmap Frame { get; set; }

        public FrameUpdateEventArgs(WriteableBitmap frame)
        {
            Frame = frame;
        }
    }
}
