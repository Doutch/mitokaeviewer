﻿using System.Windows.Media.Imaging;
using OpenCvSharp;

namespace MitoKaeViewer.Services
{
    public interface IImageDrawService : System.ComponentModel.INotifyPropertyChanged
    {
        /// <summary>
        /// 描画画像
        /// </summary>
        WriteableBitmap DrewImage { get; set; }

        /// <summary>
        /// 指定位置に矩形とスコア情報をを描画する
        /// </summary>
        /// <param name="mat">描画先の画像</param>
        /// <param name="objectInfo">検出情報</param>
        /// <param name="rectThickness">描画枠の太さ</param>
        void DrawRect(Mat mat, ObjectInfo objectInfo, int rectThickness = 3);
    }
}