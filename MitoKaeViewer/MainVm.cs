﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Reactive.Bindings;
using MitoKaeViewer.Services;
using Reactive.Bindings.Extensions;
using System.Reactive.Linq;
using System.Reactive.Disposables;

namespace MitoKaeViewer
{
    public class MainVm : Prism.Mvvm.BindableBase, IDisposable
    {
        #region Properties
        public ReactiveCommand SaveCommand { get; }
        public ReactiveProperty<bool> CaptureButtonStatus { get; }
        public ReactiveProperty<System.Windows.Media.Imaging.WriteableBitmap> ImageViewSource { get; }
        public ReactiveProperty<string> MitoAnswer { get; }
        #endregion
        private IImageCaptureService _CaptureService;
        private IFindObjectAndDrawService _FindAndDrawService;
        private CompositeDisposable _Dispose = new CompositeDisposable();

        public MainVm()
        {
            //Service初期化
            var settings = Properties.Settings.Default;
            _CaptureService = new ImageCaptureService(
                settings.CaptureResolutionWidth,
                settings.CaptureResolutionHeight,
                2.2,
                settings.RefreshSpanMs,
                settings.UseCameraSource);
            _FindAndDrawService = new FindObjectAndDrawService(
                new ObjectFindService(),
                new ImageDrawService(),
                Properties.Settings.Default.RectThickness);

            //プロパティ初期化
            CaptureButtonStatus = new ReactiveProperty<bool>(false).AddTo(_Dispose);

            //問い合わせ処理
            _CaptureService.ObserveProperty(x => x.LastCapturedMat).Where(x => x != null).Subscribe(x => _FindAndDrawService.FindAndDraw(x)).AddTo(_Dispose);

            //表示更新処理
            //描画結果
            var drawUpdateFrame = _FindAndDrawService.ObserveProperty(x => x.DrewImage);
            //フレーム更新結果
            var capUpdateFrame = Observable.FromEventPattern<FrameUpdateEventArgs>(
                x => _CaptureService.FrameUpdateEvent += x, x => _CaptureService.FrameUpdateEvent -= x).
                Select(x => x.EventArgs.Frame);
            ImageViewSource = capUpdateFrame.Merge(drawUpdateFrame).ToReactiveProperty().AddTo(_Dispose);
            //ミトのセリフ更新
            MitoAnswer = _FindAndDrawService.ObserveProperty(x => x.ObjectName).ToReactiveProperty().AddTo(_Dispose);

            //ほぞん
            SaveCommand = _FindAndDrawService.ObserveProperty(x => x.DrewImage).Select(x => x != null).ToReactiveCommand().AddTo(_Dispose);
            SaveCommand.Subscribe(_ => _FindAndDrawService.SaveLastDrewImage($"SaveImages/"));

            //キャプチャ処理
            CaptureButtonStatus.Where(x => !x).Subscribe(async _ => await _CaptureService.Start()).AddTo(_Dispose);
            CaptureButtonStatus.Where(x => x).Subscribe(_ => _CaptureService.Stop()).AddTo(_Dispose);

            //_CaptureService.Start();
        }

        public void Dispose()
        {
            _Dispose.Dispose();
        }
    }
}
